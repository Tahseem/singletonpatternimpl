
public class EagerInitSingleton {

	private static EagerInitSingleton eagerInstance=new EagerInitSingleton();
	
	private EagerInitSingleton() {};
	
	public static EagerInitSingleton getEagerInstance() {
		return eagerInstance;
	}
	
	
}
