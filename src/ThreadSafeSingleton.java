
public class ThreadSafeSingleton {

	private static ThreadSafeSingleton threadSafeInstance=null;
	
	private ThreadSafeSingleton() {};
	
	public static synchronized ThreadSafeSingleton getThreadSafeSingletonInstance() {
		if(threadSafeInstance == null) {
			threadSafeInstance=new ThreadSafeSingleton();
		}
		return threadSafeInstance;
	}
}
