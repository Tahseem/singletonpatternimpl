
public class LazyInitSingleton {

	private static LazyInitSingleton lazyInstance=null;
	private LazyInitSingleton() {};
	public static LazyInitSingleton getLazyInstance() {
		if(lazyInstance == null) {
			lazyInstance=new LazyInitSingleton();
		}
		return lazyInstance;
	}
}
