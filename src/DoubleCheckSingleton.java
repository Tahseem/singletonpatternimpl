
public class DoubleCheckSingleton {

	private static DoubleCheckSingleton doubleCheckSingletonInstance=null;
	
	private DoubleCheckSingleton() {};
	
	public static DoubleCheckSingleton getDoubleCheckSingletonInstance() {
		if(doubleCheckSingletonInstance == null) {
			synchronized (DoubleCheckSingleton.class) {
				if(doubleCheckSingletonInstance == null) {
					doubleCheckSingletonInstance =new DoubleCheckSingleton();
				}
			}
			
		}
		return doubleCheckSingletonInstance;
	}
}
